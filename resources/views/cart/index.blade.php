@extends('layouts.app')
@section('title', 'Корзина')
@section('content')
    <div class="cart_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cart_container">
                        <div class="cart_title">Ваша корзина</div>
                        @if(!$cart->isEmpty())
                            <div class="cart_items">
                                <ul class="cart_list">
                                    @foreach($cart as $item)
                                        <li class="cart_item clearfix">
                                            <div class="cart_item_image"><img src="{{$item->options->image ? asset('images/products/thumbnail/' . $item->options->image)
                                            : 'https://imgholder.ru/133x133/8693a7/adb8ca&text=NO+PHOTO&font=ptsansi'}}"
                                                                              alt=""></div>
                                            <div
                                                class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                                <div class="cart_item_name cart_info_col" style="width: 25%">
                                                    <div class="cart_item_title">Name</div>
                                                    <div class="cart_item_text">{{ $item->name }}</div>
                                                </div>
                                                <div class="cart_item_quantity cart_info_col">
                                                    <div class="cart_item_title" style="margin-bottom: 30px">Quantity
                                                    </div>
                                                    <form action="{{ route('update') }}">
                                                        @csrf
                                                        <input type="number" min="0" name="qty"
                                                               value="{{ $item->qty }}">
                                                        <input type="hidden" name="rowId" value="{{ $item->rowId }}">
                                                        <button class="btn btn-success" type="submit">
                                                            <i class="fas fa-sync"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                                <div class="cart_item_price cart_info_col">
                                                    <div class="cart_item_title">Price</div>
                                                    <div class="cart_item_text" id="itemPrice">{{ $item->price }}
                                                        &#x20bd;
                                                    </div>
                                                </div>
                                                <div class="cart_item_total cart_info_col">
                                                    <div class="cart_item_title">Total</div>
                                                    <div class="cart_item_text">{{ $item->subtotal }} &#x20bd;</div>
                                                </div>
                                                <div class="cart_item_total cart_info_col">
                                                    <div class="cart_item_title">Action</div>
                                                    <div class="cart_item_text">
                                                        <a class="btn btn-sm rounded btn-outline-danger"
                                                           href="{{ url('cart/remove/' . $item->rowId) }}">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <!-- Order Total -->
                            <div class="order_total">
                                <div class="order_total_content text-md-right">
                                    <div class="order_total_title">Order Total:</div>
                                    <div class="order_total_amount">{{ \Cart::initial() }} &#x20bd;</div>
                                </div>
                            </div>
                            <div class="order_total">
                                <div class="order_total_content text-md-right">
                                    <div class="order_total_title">С учетом доставки:</div>
                                    <div class="order_total_amount">{{ \Cart::total() }} &#x20bd;</div>
                                </div>
                            </div>

                            <div class="cart_buttons">
                                <a class="button cart_button_clear" href="{{ route('clear-cart') }}">Очистить
                                    корзину</a>
                                <a class="button cart_button_checkout" href="{{ route('place-order') }}">Оформить заказ</a>
                            </div>
                        @else
                            <p class="mt-5">Здесь пока пусто. Загляните в каталог и добавьте несколько товаров.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('frontend-asset/js/cart_custom.js')}}"></script>
    <script src="{{asset('frontend-asset/js/product_custom.js')}}"></script>
    <script src="https://shaack.com/projekte/bootstrap-input-spinner/src/bootstrap-input-spinner.js"></script>
    <script>
        $("input[type='number']").inputSpinner({
            buttonsWidth: "1.5rem",
            buttonsClass: "btn-outline-success",
        })
    </script>
@endsection

