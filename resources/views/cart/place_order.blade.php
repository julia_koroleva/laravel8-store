@extends('layouts.app')
@section('title', 'Оформление заказа')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="mt-5 mb-5">Оформление заказа</h2>
            </div>
            <div class="col-lg-12 mt-5 mb-5">
                <table class="table">
                    <caption class="text-right">Сумма заказа с учетом доставки: {{ \Cart::total() }} &#x20bd;</caption>
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Наименование</th>
                        <th scope="col">Цена</th>
                        <th scope="col">Количество</th>
                        <th scope="col">Сумма</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($cart as $item)
                        <tr>
                            <th scope="row"> {{ $loop->iteration }}</th>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ $item->qty }}</td>
                            <td>{{ $item->subtotal }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-lg-8 m-auto">
                <form action="{{ route('add-order') }}" method="post" name="full_name">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                               id="name" placeholder="ФИО" value="{{ old('name') }}">
                        @error('name')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                               id="email" placeholder="Email" value="{{ old('email') }}">
                        @error('email')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="tel" name="phone" id="phone"
                               class="form-control @error('phone') is-invalid @enderror"
                               placeholder="Телефон" value="{{ old('phone') }}">
                        @error('phone')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <textarea class="form-control @error('address') is-invalid @enderror" rows="5" name="address"
                                  id="address" placeholder="Индекс, адрес доставки">
                            {{ old('address') }}
                        </textarea>
                        @error('address')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="text-center ">
                        <button type="submit" class="btn btn-block btn-outline-success">Оформить заказ</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
