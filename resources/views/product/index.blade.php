@extends('layouts.app')
@section('title', $product->name)
@section('content')
    <div class="single_product">
        <div class="container">
            <div class="row">

                <!-- Images -->
                <div class="col-lg-2 order-lg-1 order-2">
                    <ul class="image_list">
                        <li data-image="images/single_4.jpg"><img
                                src="https://imgholder.ru/133x133/8493a8/adb9ca&text=IMAGE+HOLDER&font=kelson" alt="">
                        </li>
                        <li data-image="images/single_2.jpg"><img
                                src="https://imgholder.ru/133x133/8493a8/adb9ca&text=IMAGE+HOLDER&font=kelson" alt="">
                        </li>
                        <li data-image="images/single_3.jpg"><img
                                src="https://imgholder.ru/133x133/8493a8/adb9ca&text=IMAGE+HOLDER&font=kelson" alt="">
                        </li>
                    </ul>
                </div>

                <!-- Selected Image -->
                <div class="col-lg-5 order-lg-2 order-1">
                    <div class="image_selected"><img
                            src="{{$product->image_one ? asset('images/products/' . $product->image_one) : 'https://imgholder.ru/440x493/8493a8/adb9ca&text=IMAGE+HOLDER&font=kelson'}}"
                            alt="{{ $product->title }}"></div>
                </div>

                <!-- Description -->
                <div class="col-lg-5 order-3">
                    <div class="product_description">
                        <div class="product_category"><a
                                href="{{route('category', $product->category->id)}}">{{ $product->category->name }}</a>
                        </div>
                        <div class="product_name">{{ $product->title }}</div>
                        <div class="product_text"><p>
                                {!! $product->description !!}
                            </p></div>
                        <div class="order_info d-flex flex-row">
                            <form action="{{ url('cart/add/'. $product->id) }}" method="post">
                                @csrf
                                <div class="" style="z-index: 1000;">
                                    <!-- Product Quantity -->
                                    <label for="qty">Quantity</label>
                                    <input class="form-control form-control-lg" style="width: 90px;" id="qty" type="number"
                                           name="product_quantity" value="1">
                                </div>

                                <div class="product_price">{{ $product->price }} &#x20bd;</div>
                                <div class="button_container">
                                    <button type="button" class="button cart_button addcart"
                                            data-id="{{ $product->id }}">Add to Cart
                                    </button>
                                    <div class="product_fav"><i class="fas fa-heart"></i></div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('frontend-asset/js/product_custom.js')}}"></script>
    <script>
        $('.addcart').on('click', function () {
            let id = $(this).data('id');
            let qty = $('#qty').val();

            if (id) {
                $.ajax({
                    url: "{{ url('cart/add/') }}/" + id,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        product_quantity: qty,
                    },
                    type: "POST",
                    dataType: "json",
                    success: function (data) {
                        let count = $('.cart_count span');
                        let subtotal = $('.cart_price');
                        count.text('');
                        subtotal.text('');
                        count.text(data.countProduct);
                        subtotal.html(data.cartSubtotal + ' &#x20bd;');
                        alert(data.cart.name + ' added to cart');
                    }
                })
            }
        })
    </script>
@endsection
