@extends('layouts.app')
@section('title', 'Ваши заказы')
@section('content')
    <div class="cart_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cart_container">
                        <div class="cart_title mb-3">Ваши заказы</div>
                        @if(!$orders->isEmpty())
                            @foreach($orders as $order)
                                <h3 class="title mb-1">Заказ № {{ $order->id }},
                                    создан {{ \Carbon\Carbon::parse($order->created_at)->diffForHumans()}}</h3>
                                <p>Статус:
                                    <span class="badge badge-pill badge-primary mb-2">
                                    @if($order->status == 1)
                                            Создан
                                        @endif</span>
                                </p>

                                <table class="table">
                                    <caption class="text-right">Сумма заказа с учетом доставки: {{ $order->total_price }} &#x20bd;</caption>
                                    <thead>
                                    <tr>
                                        <th scope="col">Наименование</th>
                                        <th scope="col">Цена</th>
                                        <th scope="col">Количество</th>
                                        <th scope="col">Сумма</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($order->content as $orderInfo)
                                        <tr>
                                            <td>{{ $orderInfo->name }}</td>
                                            <td>{{ $orderInfo->price }}</td>
                                            <td>{{ $orderInfo->qty }}</td>
                                            <td>{{ $orderInfo->subtotal }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endforeach
                        @else
                            <p class="mt-5">Здесь пока пусто. Загляните в каталог и добавьте несколько товаров.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('frontend-asset/js/cart_custom.js')}}"></script>
    <script src="{{asset('frontend-asset/js/product_custom.js')}}"></script>
@endsection


