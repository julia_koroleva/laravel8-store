@extends('layouts.app')
@section('title', $oneCategory->name)
@section('content')
    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll"
             data-image-src="images/shop_background.jpg"></div>
        <div class="home_overlay"></div>
        <div class="home_content d-flex flex-column align-items-center justify-content-center">
            <h2 class="home_title">{{$oneCategory->name}}</h2>
        </div>
    </div>

    <div class="shop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">

                    <!-- Shop Sidebar -->
                    <div class="shop_sidebar">
                        <div class="sidebar_section">
                            <div class="sidebar_title">Categories</div>
                            <ul class="sidebar_categories">
                                @foreach($categories as $category)
                                    <li><a href="{{route('category', $category->id)}}">{{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="sidebar_section filter_by_section">
                            <div class="sidebar_title">Filter By</div>
                            <div class="sidebar_subtitle">Price</div>
                            <div class="filter_price">
                                <div id="slider-range" class="slider_range"></div>
                                <p>Range: </p>
                                <p><input type="text" id="amount" class="amount" readonly
                                          style="border:0; font-weight:bold;"></p>
                            </div>
                        </div>
                        <div class="sidebar_section">
                            <div class="sidebar_subtitle color_subtitle">Color</div>
                            <ul class="colors_list">
                                <li class="color"><a href="#" style="background: #b19c83;"></a></li>
                                <li class="color"><a href="#" style="background: #000000;"></a></li>
                                <li class="color"><a href="#" style="background: #999999;"></a></li>
                                <li class="color"><a href="#" style="background: #0e8ce4;"></a></li>
                                <li class="color"><a href="#" style="background: #df3b3b;"></a></li>
                                <li class="color"><a href="#"
                                                     style="background: #ffffff; border: solid 1px #e1e1e1;"></a></li>
                            </ul>
                        </div>
                        <div class="sidebar_section">
                            <div class="sidebar_subtitle brands_subtitle">Brands</div>
                            <ul class="brands_list">
                                @foreach($brands as $brand)
                                    <li class="brand"><a href="#">{{ $brand->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="col-lg-9">

                    <!-- Shop Content -->

                    <div class="shop_content">
                        <div class="shop_bar clearfix">
                            <div class="shop_product_count"><span>{{ $productsCount }}</span> products found</div>
                            <div class="shop_sorting">
                                <span>Sort by:</span>
                                <ul>
                                    <li>
                                        <span class="sorting_text">highest rated<i
                                                class="fas fa-chevron-down"></i></span>
                                        <ul>
                                            <li class="shop_sorting_button"
                                                data-isotope-option='{ "sortBy": "original-order" }'>highest rated
                                            </li>
                                            <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>
                                                name
                                            </li>
                                            <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "price" }'>
                                                price
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="product_grid d-flex justify-content-between">
                            <!-- Product Item -->
                            @if(!$oneCategory->products->isEmpty())
                                @foreach($oneCategory->products as $product)
                                    <div class="product_item is_new">
                                        <div class="product_border"></div>
                                        <div
                                            class="product_image d-flex flex-column align-items-center justify-content-center mb-2">
                                            <img class="img-fluid" style="max-width: 100%; height: auto"
                                                 src="{{$product->image_one ? asset('images/products/thumbnail/' . $product->image_one)
                                                    : 'https://imgholder.ru/177,5x177,5/8493a8/adb9ca&text=NO+PHOTO&font=matias'}}"
                                                 alt="{{ $product->title }}">
                                        </div>
                                        <div class="product_content">
                                            <div class="product_price">{{ $product->price }} &#x20bd;</div>
                                            <div class="product_name">
                                                <div><a href="{{ route('product', $product->id) }}"
                                                        tabindex="0">{{ $product->title }}</a></div>
                                            </div>
                                        </div>
                                        <a id="add-wishlist" data-id="{{ $product->id }}"
                                           href="{{url('wishlist/add/' . $product->id)}}">
                                            <div class="product_fav"><i class="fas fa-heart"></i></div>
                                        </a>
                                        <ul class="product_marks">
                                            <li class="product_mark product_discount">-25%</li>
                                            <li class="product_mark product_new">new</li>
                                        </ul>
                                    </div>
                                @endforeach
                            @else
                                <p class="m-3">В данной категории пока нет ни одного товара</p>
                            @endif
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#add-wishlist').on('click', function (e) {
            e.preventDefault();
            let id = $(this).data('id');
            if (id) {
                $.ajax({
                    url: "{{ url('wishlist/add/') }}/" + id,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        let count = $('.wishlist_count');
                        count.text('');
                        count.text(data.countProduct);
                        alert(data.wishlist.name + ' ' + data.success);
                    }
                })
            }
        })
    </script>
@endsection
