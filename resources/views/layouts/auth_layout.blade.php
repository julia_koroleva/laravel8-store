<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ config('app.name') }}</title>

    <!-- vendor css -->
    <link href="{{asset('admin-asset/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('admin-asset/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('admin-asset/lib/select2/css/select2.min.css')}}" rel="stylesheet">


    <!-- Starlight CSS -->
    <link rel="stylesheet" href="{{asset('admin-asset/css/starlight.css')}}">
</head>

<body>

<div class="d-flex align-items-center justify-content-center bg-sl-primary ht-md-100v">

    @yield('content')

</div><!-- d-flex -->

<script src="{{asset('admin-asset/lib/jquery/jquery.js')}}"></script>
<script src="{{asset('admin-asset/lib/popper.js/popper.js')}}"></script>
<script src="{{asset('admin-asset/lib/bootstrap/bootstrap.js')}}"></script>
<script src="{{asset('admin-asset/lib/select2/js/select2.min.js')}}"></script>
<script>
    $(function(){
        'use strict';

        $('.select2').select2({
            minimumResultsForSearch: Infinity
        });
    });
</script>

</body>
</html>

