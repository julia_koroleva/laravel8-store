<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ config('app.name') }}</title>

    <!-- vendor css -->
    <link href="{{asset('admin-asset/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('admin-asset/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('admin-asset/lib/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet">
    <link href="{{asset('admin-asset/lib/rickshaw/rickshaw.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin-asset/lib/highlightjs/github.css')}}" rel="stylesheet">
    <link href="{{asset('admin-asset/lib/datatables/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('admin-asset/lib/select2/css/select2.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/toastr.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"/>
    <script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>
    <!-- Starlight CSS -->
    <link rel="stylesheet" href="{{asset('admin-asset/css/starlight.css')}}">
</head>

<body>

<!-- ########## START: LEFT PANEL ########## -->
<div class="sl-logo"><a href="{{route('home')}}"><i class="icon ion-android-star-outline"></i> test store</a></div>
<div class="sl-sideleft">
    <div class="input-group input-group-search">
        <input type="search" name="search" class="form-control" placeholder="Search">
        <span class="input-group-btn">
          <button class="btn"><i class="fa fa-search"></i></button>
        </span><!-- input-group-btn -->
    </div><!-- input-group -->

    <label class="sidebar-label">Navigation</label>
    <div class="sl-sideleft-menu">
        <a href="{{route('admin')}}" class="sl-menu-link active">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Home</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->

        <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i>
                <span class="menu-item-label">Category & Brands</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{route('category.index')}}" class="nav-link">Category</a></li>
            <li class="nav-item"><a href="{{route('brand.index')}}" class="nav-link">Brand</a></li>
        </ul>

        <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i>
                <span class="menu-item-label">Products</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{route('product.index')}}" class="nav-link">All Products</a></li>
            <li class="nav-item"><a href="{{route('product.create')}}" class="nav-link">Add new product</a></li>
        </ul>
    </div><!-- sl-sideleft-menu -->
    <br>
</div><!-- sl-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<div class="sl-header">
    <div class="sl-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a>
        </div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a>
        </div>
    </div><!-- sl-header-left -->
    <div class="sl-header-right">
        <nav class="nav">
            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name">Jane<span class="hidden-md-down"> Doe</span></span>
                    <img src="{{asset('admin-asset/img/img3.jpg')}}" class="wd-32 rounded-circle" alt="">
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-200">
                    <ul class="list-unstyled user-profile-nav">
                        <li><a href=""><i class="icon ion-ios-person-outline"></i> Edit Profile</a></li>
                        <li><a href="{{route('sign-out')}}"><i class="icon ion-power"></i> Sign Out</a></li>
                    </ul>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>
    </div><!-- sl-header-right -->
</div><!-- sl-header -->
<!-- ########## END: HEAD PANEL ########## -->

<!-- ########## START: MAIN PANEL ########## -->
<div class="sl-mainpanel">
    @yield('breadcrumbs')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>@yield('title')</h5>
        </div><!-- sl-page-title -->
        @yield('content')
    </div>
</div><!-- sl-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="{{asset('admin-asset/lib/jquery/jquery.js')}}"></script>
<script src="{{asset('admin-asset/lib/popper.js/popper.js')}}"></script>
<script src="{{asset('admin-asset/lib/bootstrap/bootstrap.js')}}"></script>
<script src="{{asset('admin-asset/lib/jquery-ui/jquery-ui.js')}}"></script>
<script src="{{asset('admin-asset/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
<script src="{{asset('admin-asset/lib/jquery.sparkline.bower/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('admin-asset/lib/d3/d3.js')}}"></script>
<script src="{{asset('admin-asset/lib/rickshaw/rickshaw.min.js')}}"></script>
<script src="{{asset('admin-asset/lib/chart.js/Chart.js')}}"></script>
<script src="{{asset('admin-asset/lib/Flot/jquery.flot.js')}}"></script>
<script src="{{asset('admin-asset/lib/Flot/jquery.flot.pie.js')}}"></script>
<script src="{{asset('admin-asset/lib/Flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('admin-asset/lib/flot-spline/jquery.flot.spline.js')}}"></script>
<script src="{{asset('admin-asset/lib/highlightjs/highlight.pack.js')}}"></script>
<script src="{{asset('admin-asset/lib/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin-asset/lib/datatables-responsive/dataTables.responsive.js')}}"></script>
<script src="{{asset('admin-asset/lib/select2/js/select2.min.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script>
    $(function () {
        'use strict';

        $('#datatable').DataTable({
            responsive: true,
            language: {
                searchPlaceholder: 'Поиск...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        // Select2
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});

    });
</script>

<script src="{{asset('admin-asset/js/starlight.js')}}"></script>
<script src="{{asset('admin-asset/js/ResizeSensor.js')}}"></script>
<script src="{{asset('admin-asset/js/dashboard.js')}}"></script>
<script src="{{asset('js/toastr.min.js')}}"></script>
<script>
    @if(Session::has('message'))
    let type = "{{Session::get('alert-type', 'info')}}";
    switch (type) {
        case 'info':
            toastr.info("{{Session::get('message')}}");
            break;
        case 'success':
            toastr.success("{{Session::get('message')}}");
            break;
        case 'warning':
            toastr.warning("{{Session::get('message')}}");
            break;
        case 'error':
            toastr.error("{{Session::get('message')}}");
            break;
    }
    @endif
</script>
<script>
    $(document).on('click', '.edit', function () {
        let _this = $(this).parents('tr');
        $('#m_id').val(_this.find('#id').data('id')).text();
        $('#m_name').val(_this.find('#name').data('name')).text();
        $('#m_parent').val(_this.find('#parent').data('parent')).text();
        $('#m_logo').val(_this.find('#logo').data('src')).text();
    })
</script>
@yield('scripts')
</body>
</html>

