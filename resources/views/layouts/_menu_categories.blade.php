<div class="cat_menu_container">
    <div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
        <div class="cat_burger"><span></span><span></span><span></span></div>
        <div class="cat_menu_text">categories</div>
    </div>
    <ul class="cat_menu d-none">
        @foreach($categories as $category)
            <li class="hassubs">
                <a href="{{$category->children->isEmpty() ? route('category', $category->id) : '#'}}">
                    {{$category->name}} @if(!$category->children->isEmpty())<i class="fas fa-chevron-right"></i>@endif
                </a>
                <ul>
                    @foreach($category->children as $child)
                        <li class="hassubs">
                            <a href="{{route('category', $child->id)}}">{{$child->name}}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>
</div>
