@extends('layouts.auth_layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header mb-3">
                        <div class="signin-logo tx-center tx-24 tx-bold tx-inverse">Test <span
                                class="tx-info tx-normal">Store</span></div>
                    </div>

                    <div class="card-body pd-35-force">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                       placeholder="Enter your username"
                                       name="name" value="{{ old('name') }}"
                                       required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                       placeholder="Enter your email"
                                       name="email"
                                       value="{{ old('email') }}" required autocomplete="email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                       name="password"
                                       required autocomplete="new-password"
                                       placeholder="Enter your password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input type="password" class="form-control"
                                       name="password_confirmation"
                                       required autocomplete="new-password"
                                       placeholder="Confirm your password">
                            </div><!-- form-group -->

                            <button type="submit" class="btn btn-info btn-block">{{ __('Register') }}</button>
                        </form>
                        <div class="mg-t-40 tx-center">Already have an account? <a href="{{route('login')}}" class="tx-info">Sign In</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
