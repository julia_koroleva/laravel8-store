@extends('layouts.admin_layout')
@section('breadcrumbs')
    <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{url('admin')}}">Home Admin</a>
        <span class="breadcrumb-item active">Category</span>
    </nav>
@endsection
@section('title', 'Category Table')
@section('content')
    <div class="card pd-20 pd-sm-40">
        <h6 class="card-body-title">All Categories
            <a class="btn btn-sm btn-outline-dark" href="#" style="float: right" data-toggle="modal"
               data-target="#modal-create">Add Category</a>
        </h6>

        <div class="table-wrapper">
            <table id="datatable" class="table display responsive nowrap">
                <thead>
                <tr>
                    <th class="wd-15p">#</th>
                    <th class="wd-15p">Category name</th>
                    <th class="wd-20p">Parent Category</th>
                    <th class="wd-15p">Created at</th>
                    <th class="wd-10p">Updated At</th>
                    <th class="wd-10p">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $key => $item)
                    <tr>
                        <td id="id" data-id="{{$item->id}}">{{$key + 1}}</td>
                        <td id="name" data-name="{{$item->name}}">{{$item->name}}</td>
                        <td id="parent"
                            data-parent="{{$item->parent_id}}">{{\App\Models\Category::find($item->parent_id)->name ?? 'no parent'}}</td>
                        <td class="">{{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}</td>
                        <td class="">{{\Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans()}}</td>
                        <td>
                            <div class="btn-group">
                                <button style="float: right" data-toggle="modal"
                                        data-target="#modal-edit" class="btn btn-primary btn-icon mr-2 edit">
                                    <div><i class="fa fa-cog"></i></div>
                                </button>
                                <form action="{{route('category.destroy', ['category' => $item->id])}}"
                                      method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-icon"
                                            onclick="return confirm('Удалить категорию?')">
                                        <div><i class="fa fa-trash"></i></div>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- table-wrapper -->
    </div><!-- card -->
    <!-- CREATE MODAL -->
    <div id="modal-create" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add new category</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-40">
                    <form method="post" action="{{route('category.store')}}">
                        @csrf
                        <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
                            <div class="row">
                                <input type="text" class="form-control"
                                       placeholder="Enter category name"
                                       name="name">
                            </div><!-- row -->
                            <div class="row mg-t-20">
                                <select class="form-control select2"
                                        data-placeholder="Choose Category"
                                        name="parent_id">
                                    <option value="0" selected disabled>Chose parent category</option>
                                    @foreach($parentCategories as $parent)
                                        <option value="{{$parent->id}}">{{$parent->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-layout-footer mg-t-20">
                                <button type="submit" class="btn btn-info mg-r-5">Submit category</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div><!-- form-layout-footer -->
                        </div>
                    </form>
                </div><!-- modal-body -->
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    @if(!$categories->isEmpty())
        <!-- UPDATE MODAL -->
        <div id="modal-edit" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add new category</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body pd-40">
                        <form method="post" action="{{route('category.update', ['category' => $item->id])}}">
                            @csrf
                            @method('PUT')
                            <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
                                <div class="row">
                                    <input type="text" class="form-control"
                                           placeholder="Enter category name"
                                           name="name"
                                           id="m_name">
                                </div><!-- row -->
                                <div class="row mg-t-20">
                                    <select class="form-control select2"
                                            data-placeholder="Choose Category"
                                            name="parent_id"
                                            id="m_parent">
                                        <option value="0" selected disabled>Chose parent category</option>
                                        @foreach($parentCategories as $parent)
                                            <option value="{{$parent->id}}">{{$parent->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="hidden" name="id" id="m_id">
                                <div class="form-layout-footer mg-t-20">
                                    <button type="submit" class="btn btn-info mg-r-5">Submit category</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div><!-- form-layout-footer -->
                            </div>
                        </form>
                    </div><!-- modal-body -->
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    @endif
@endsection
