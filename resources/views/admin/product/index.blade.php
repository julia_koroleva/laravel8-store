@extends('layouts.admin_layout')
@section('breadcrumbs')
    <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{url('admin')}}">Home Admin</a>
        <span class="breadcrumb-item active">Product</span>
    </nav>
@endsection
@section('title', 'Product Table')
@section('content')
    <div class="card pd-20 pd-sm-40">
        <h6 class="card-body-title">All Products
            <a class="btn btn-sm btn-outline-dark" href="{{route('product.create')}}" style="float: right">Add
                Product</a>
        </h6>

        <div class="table-wrapper">
            <table id="datatable" class="table display responsive nowrap">
                <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-10p">Product title</th>
                    <th class="wd-25p">Description</th>
                    <th class="wd-5p">Code</th>
                    <th class="wd-5p">Brand</th>
                    <th class="wd-5p">Category</th>
                    <th class="wd-5p">Price</th>
                    <th class="wd-5p">Sale price</th>
                    <th class="wd-5p">Quantity</th>
                    <th class="wd-10p">Created at</th>
                    <th class="wd-10p">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $key => $item)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$item->title}}</td>
                        <td>{!! $item->description !!}</td>
                        <td>{{$item->code}}</td>
                        <td>{{$item->brand->name}}</td>
                        <td>{{$item->category->name}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{$item->discount_price}}</td>
                        <td>{{$item->quantity}}</td>
                        <td class="">{{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{route('product.show', ['product' => $item->id])}}" style="float: right"
                                   class="btn btn-primary btn-icon mr-2">
                                    <div><i class="fa fa-eye"></i></div>
                                </a>
                                <form action="{{route('product.destroy', ['product' => $item->id])}}"
                                      method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-icon"
                                            onclick="return confirm('Удалить продукт?')">
                                        <div><i class="fa fa-trash"></i></div>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- table-wrapper -->
    </div><!-- card -->
@endsection

