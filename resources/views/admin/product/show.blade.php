@extends('layouts.admin_layout')
@section('breadcrumbs')
    <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{url('admin')}}">Home Admin</a>
        <a class="breadcrumb-item" href="{{url('admin.product.index')}}">All Products</a>
        <a class="breadcrumb-item" href="#">{{$product->category->name}}</a>
        <span class="breadcrumb-item active">{{$product->title}}</span>
    </nav>
@endsection
@section('title', $product->title)
@section('content')
    <div class="card m-2 p-5">
        <div class="card-body-title mg-b-10-force">
            {{$product->title}}
        </div>
        <div class="row">
            <div class="col-6">
                <img src="{{asset('images/products/thumbnail/' . $product->image_one)}}" alt="">
            </div>
            <div class="col-6">
                <div class="mb-2">
                    <div class="text-uppercase text-dark">Brand:</div>
                    {{$product->brand->name}}
                </div>
                <div class="mb-2">
                    <div class="text-uppercase text-dark">Product code:</div>
                    {{$product->code}}
                </div>

                <div class="mb-2">
                    <div class="text-uppercase text-dark">Product quantity:</div>
                    {{$product->quantity}}
                </div>
                <div class="mb-2">
                    <div class="text-uppercase text-dark">Product price:</div>
                    {{$product->price}}
                </div>
                <div class="mb-2">
                    <div class="text-uppercase text-dark">Sale price:</div>
                    {{$product->discount_price}}
                </div>
                <div class="mb-2">
                    <div class="text-uppercase text-dark">Category:</div>
                    {{$product->category->name}}
                </div>
                <div class="mb-2">
                    <div class="text-uppercase text-dark">Description:</div>
                    {!! $product->description !!}
                </div>
            </div>
        </div>
    </div>
@endsection
