@extends('layouts.admin_layout')

@section('breadcrumbs')
    <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{url('admin')}}">Home Admin</a>
        <span class="breadcrumb-item active">Add Product</span>
    </nav>
@endsection
@section('title', 'Add Product')
@section('content')
    <div class="card pd-20 pd-sm-40">
        <h6 class="card-body-title">
            Add new product
            <a class="btn btn-success btn-sm pull-right" href="{{route('product.index')}}">All products</a>
        </h6>
        <p class="mg-b-20 mg-sm-b-30">Add a new product to the database.</p>
        <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-layout">
                <div class="row mg-b-25">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Product title: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="title" value=""
                                   placeholder="Enter product title">
                        </div>
                    </div><!-- title col-3 -->
                    <div class="col-lg-3">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Brand: <span class="tx-danger">*</span></label>
                            <select class="form-control select2" name="brand_id">
                                <option selected disabled value="0">Select Brand</option>
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!-- brand col-3 -->
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Product code: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="code" value=""
                                   placeholder="Enter product code">
                        </div>
                    </div><!-- code col-3 -->
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Product quantity:</label>
                            <input class="form-control" type="text" name="quantity" value=""
                                   placeholder="Enter product quantity">
                        </div>
                    </div><!-- quantity col-3 -->

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Product price:</label>
                            <input class="form-control" type="text" name="price" value=""
                                   placeholder="Enter product price">
                        </div>
                    </div><!-- price col-4 -->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Sale price:</label>
                            <input class="form-control" type="text" name="discount_price" value=""
                                   placeholder="Enter sale price">
                        </div>
                    </div><!-- sale price col-4 -->
                    <div class="col-lg-4">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Category: <span class="tx-danger">*</span></label>
                            <select class="form-control select2" name="category_id">
                                <option value="0">Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!-- category col-4 -->

                    <div class="col-lg-12">
                        <label class="form-control-label">Description:</label>
                        <textarea rows="8" class="form-control" placeholder="Add description" name="description" id="description"></textarea>
                    </div> <!-- description col-12 -->

                    <div class="col-lg-6 mg-t-20-force mg-lg-t-0">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Size:</label>
                            <select class="form-control select2" name="size" data-role="tagsinput" multiple>
                            </select>
                        </div>
                    </div><!-- size col-6 -->
                    <div class="col-lg-6 mg-t-20-force mg-lg-t-0">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Color:</label>
                            <select class="form-control select2" name="color" data-role="tagsinput" multiple>
                            </select>
                        </div>
                    </div><!-- color col-6 -->
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Video link:</label>
                            <input class="form-control" type="text" name="video_link" value=""
                                   placeholder="Enter video link">
                        </div>
                    </div><!-- video link col-12 -->
                    <div class="col-lg-4 mg-t-20-force mg-lg-t-0">
                        <label class="ckbox">
                            <input type="checkbox" name="main_slider"><span>Main Slider</span>
                        </label>
                        <label class="ckbox">
                            <input type="checkbox" name="mid_slider"><span>Mid Slider</span>
                        </label>
                    </div><!-- sliders col-4 -->
                    <div class="col-lg-4 mg-t-20-force mg-lg-t-0">
                        <label class="ckbox">
                            <input type="checkbox" name="hot_deal"><span>Hot deal</span>
                        </label>
                        <label class="ckbox">
                            <input type="checkbox" name="hot_new"><span>Hot new</span>
                        </label>
                    </div><!-- hot col-4 -->
                    <div class="col-lg-4 mg-t-20-force mg-lg-t-0">
                        <label class="ckbox">
                            <input type="checkbox" name="trand"><span>Trand</span>
                        </label>
                        <label class="ckbox">
                            <input type="checkbox" name="best_rated"><span>Best rated</span>
                        </label>
                    </div><!-- trend & best rated col-4 -->
                    <div class="col-lg-4 mg-t-20-force mg-lg-t-0">
                        <label for="image_one">Image</label>
                        <input type="file" class="form-control @error('image_one') is-invalid @enderror"
                               name="image_one"
                               id="image_one">
                        @error('logo')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div><!-- file 1 col-4 -->
                </div><!-- row -->

                <div class="form-layout-footer">
                    <button type="submit" class="btn btn-info mg-r-5">Submit Form</button>
                    <button class="btn btn-secondary">Cancel</button>
                </div><!-- form-layout-footer -->
            </div><!-- form-layout -->
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        ClassicEditor
            .create(document.querySelector('#description'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
