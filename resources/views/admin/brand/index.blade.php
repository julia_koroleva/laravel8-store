@extends('layouts.admin_layout')
@section('breadcrumbs')
    <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{url('admin')}}">Home Admin</a>
        <span class="breadcrumb-item active">Brand</span>
    </nav>
@endsection
@section('title', 'Brands Table')
@section('content')
    <div class="card pd-20 pd-sm-40">
        <h6 class="card-body-title">All Brands
            <a class="btn btn-sm btn-outline-dark" href="#" style="float: right" data-toggle="modal"
               data-target="#modal-create">Add Brand</a>
        </h6>

        <div class="table-wrapper">
            <table id="datatable" class="table display responsive nowrap">
                <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-15p">Brand name</th>
                    <th class="wd-15p">Brand logo</th>
                    <th class="wd-15p">Created at</th>
                    <th class="wd-10p">Updated At</th>
                    <th class="wd-10p">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($brands as $key => $item)
                    <tr>
                        <td id="id" data-id="{{$item->id}}">{{$key + 1}}</td>
                        <td id="name" data-name="{{$item->name}}">{{$item->name}}</td>
                        <td id="logo"><img src="{{asset('images/brands/thumbnail' .'/'. $item->logo)}}" alt="{{$item->name}}">
                        </td>
                        <td class="">{{\Illuminate\Support\Carbon::parse($item->created_at)->format('d.m.Y')}}</td>
                        <td class="">{{\Illuminate\Support\Carbon::parse($item->updated_at)->format('d.m.Y')}}</td>
                        <td>
                            <div class="btn-group">
                                <button style="float: right" data-toggle="modal"
                                        data-target="#modal-edit" class="btn btn-primary btn-icon mr-2 edit">
                                    <div><i class="fa fa-cog"></i></div>
                                </button>
                                <form action="{{route('brand.destroy', ['brand' => $item->id])}}"
                                      method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-icon"
                                            onclick="return confirm('Удалить бренд?')">
                                        <div><i class="fa fa-trash"></i></div>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- table-wrapper -->
    </div><!-- card -->
    <!-- CREATE MODAL -->
    <div id="modal-create" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add new brand</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-40">
                    <form method="post" action="{{route('brand.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
                            <div class="row">
                                <label for="name">Название</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                       placeholder="Enter brand name"
                                       name="name"
                                       id="name">
                                @error('name')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="row mt-2">
                                <label for="logo">Логотип</label>
                                <input type="file" class="form-control @error('logo') is-invalid @enderror"
                                       name="logo"
                                       id="logo">
                                @error('logo')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div><!-- row -->
                            <div class="form-layout-footer mg-t-20">
                                <button type="submit" class="btn btn-info mg-r-5">Add brand</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div><!-- form-layout-footer -->
                        </div>
                    </form>
                </div><!-- modal-body -->
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    @if(!$brands->isEmpty())
        <!-- EDIT MODAL -->
        <div id="modal-edit" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add new brand</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body pd-40">
                        <form method="post" action="{{route('brand.update', ['brand' => $item->id])}}">
                            @csrf
                            @method('PUT')
                            <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
                                <div class="row">
                                    <label for="name">Название</label>
                                    <input type="text" class="form-control"
                                           placeholder="Enter brand name"
                                           name="name"
                                           id="m_name">
                                </div>
                                <div class="row mt-2">
                                    <label for="logo">Логотип</label>
                                    <input type="file" class="form-control"
                                           name="logo"
                                           id="m_logo">
                                </div>
                                <div class="row mt-2">
                                    <img src="{{asset('images/brands/thumbnail' .'/'. $item->logo)}}" alt="{{$item->name}}">
                                </div>
                                <!-- row -->
                                <input type="hidden" name="id" id="m_id">
                                <div class="row form-layout-footer mg-t-20">
                                    <button type="submit" class="btn btn-info mg-r-5">Submit brand</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div><!-- form-layout-footer -->
                            </div>
                        </form>
                    </div><!-- modal-body -->
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    @endif
@endsection

