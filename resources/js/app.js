import {createApp} from "vue";
import Logo from './components/Logo';
createApp(Logo).mount("#logo");

require('./bootstrap');
