<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController as AdminCategory;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Admin\ProductController as AdminProduct;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\WishlistController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/logout', [HomeController::class, 'logout'])->name('logout');
Route::get('/my-orders', [OrderController::class, 'showOrders'])->name('orders');

Route::get('category/{id}', [CategoryController::class, 'index'])->name('category');
Route::get('product/{id}', [ProductController::class, 'index'])->name('product');

Route::post('cart/add/{id}', [CartController::class, 'addtocart'])->name('addtocart');
Route::get('cart/show', [CartController::class, 'showCart'])->name('show-cart');
Route::get('cart/clear', [CartController::class, 'clearCart'])->name('clear-cart');
Route::get('cart/remove/{id}', [CartController::class, 'removeProduct'])->name('remove-product');
Route::get('cart/update', [CartController::class, 'update'])->name('update');
Route::get('cart/place-order', [CartController::class, 'placeOrder'])->name('place-order');
Route::post('cart/create-order', [CartController::class, 'addOrder'])->name('add-order');

Route::get('wishlist/add/{id}', [WishlistController::class, 'addToWishlist'])->name('add-wishlist');
Route::get('wishlist/clear', [WishlistController::class, 'clearWishlist'])->name('clear-wishlist');
Route::get('wishlist/show', [WishlistController::class, 'showWishlist'])->name('show-wishlist');
Route::get('wishlist/remove/{id}', [WishlistController::class, 'removeProduct'])->name('remove-in-wishlist');

Route::middleware(['role:admin'])->prefix('admin')->group(function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin');
    Route::get('/logout', [AdminController::class, 'logout'])->name('sign-out');
    Route::resource('/category', AdminCategory::class);
    Route::resource('/brand', BrandController::class);
    Route::resource('/product', AdminProduct::class);
});
