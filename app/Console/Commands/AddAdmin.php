<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AddAdmin extends Command
{
    protected $signature = 'add:admin';

    protected $description = 'Add user with admin role';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function handle()
    {
        $name = $this->validate_cmd(function () {
            return $this->ask('Введите имя:');
        }, ['name', 'required|min:3']);

        $email = $this->validate_cmd(function () {
            return $this->ask('Введите email:');
        }, ['email', 'required|email|unique:users']);

        $password = $this->validate_cmd(function () {
            return $this->secret('Введите пароль:');
        }, ['password', 'required|min:6']);

        if ($this->confirm('Создать пользователя?')) {
            $user = new User();
            $user->name = $name;
            $user->email = $email;
            $user->password = bcrypt($password);
            $user->email_verified_at = now();
            $user->remember_token = Str::random(10);
            $user->save();
            $user->assignRole('admin');
            $this->info('Пользователь с ролью Администратор был успешно создан');
        } else {
            $this->warn('Отмена создания');
        }
    }

    /**
     * @param mixed $method
     * @param array $rules
     * @return string
     */
    public function validate_cmd($method, array $rules): string
    {
        $value = $method();
        $validate = $this->validateInput($rules, $value);

        if ($validate !== true) {
            $this->warn($validate);
            $value = $this->validate_cmd($method, $rules);
        }
        return $value;
    }

    /**
     * @param $rules
     * @param $value
     * @return bool|string
     */
    public function validateInput($rules, $value)
    {
        $validator = Validator::make([$rules[0] => $value], [$rules[0] => $rules[1]]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return $error->first($rules[0]);
        } else {
            return true;
        }
    }
}
