<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Order;
use Auth;


class OrderController extends Controller
{
    public function showOrders()
    {
        $categories = Category::whereNull('parent_id')->with('children')->orderBy('name')->get();
        $orders = Order::where(['identifier' => Auth::id()])->get();

        foreach ($orders as $order) {
            $orderContent = json_decode($order->content);
            if (is_string($orderContent)) {
                $orderContent = json_decode($orderContent);
            }
            $order->content = $orderContent;
        }

        return view('order.show', compact('categories', 'orders'));
    }
}
