<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Redirect;

class CartController extends Controller
{
    public function addtocart(Request $request, $id)
    {
        $product = Product::find($id);
        $data = [];
        $data['id'] = $product->id;
        $data['name'] = $product->title;
        $data['qty'] = (int) $request->input('product_quantity');
        $data['price'] = $product->price;
        $data['weight'] = 1;
        $data['options']['image'] = $product->image_one;

        $cart = Cart::instance('cart')->add($data);

        $countProduct = Cart::instance('cart')->count();
        $cartSubtotal = Cart::instance('cart')->subtotal();

        return \Response::json([
            'success' => 'Added to cart',
            'cart' => $cart,
            'countProduct' => $countProduct,
            'cartSubtotal' => $cartSubtotal
        ]);
    }

    public function showCart()
    {

        $cart = Cart::instance('cart')->content();
        $categories = Category::whereNull('parent_id')->with('children')->orderBy('name')->get();

        return view('cart.index', compact('cart', 'categories'));
    }

    public function update(Request $request)
    {
        Cart::instance('cart')->update($request->rowId, $request->qty);

        $notification = [
            'message' => 'Количество товаров изменено',
            'alert-type' => 'success'
        ];

        return Redirect::back()->with($notification);

    }

    public function removeProduct($id)
    {
        Cart::instance('cart')->remove($id);

        $notification = [
            'message' => 'Товар удален из корзины',
            'alert-type' => 'success'
        ];

        return Redirect::back()->with($notification);
    }

    public function clearCart()
    {
        Cart::instance('cart')->destroy();

        $notification = [
            'message' => 'Корзина очищена',
            'alert-type' => 'success'
        ];
        return Redirect::route('home')->with($notification);
    }

    public function placeOrder()
    {
        $categories = Category::whereNull('parent_id')->with('children')->orderBy('name')->get();
        $cart = Cart::instance('cart')->content();

        return view('cart.place_order', compact('categories', 'cart'));
    }

    public function addOrder(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'address' => 'required'
        ]);

        $order = new Order([
            'full_name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'identifier' => \Auth::id(),
            'instance' => 'cart',
            'content' => json_encode(Cart::instance('cart')->content()),
            'total_price' => Cart::instance('cart')->total()
        ]);

        $order->save();

        Cart::instance('cart')->destroy();

        $notification = [
            'message' => 'Заказ успешно создан!',
            'alert-type' => 'success'
        ];
        return Redirect::route('home')->with($notification);
    }
}
