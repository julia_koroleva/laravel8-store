<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index($id)
    {
        $product = Product::find($id);

        $categories = Category::whereNull('parent_id')->with('children')->orderBy('name')->get();

        $brands = Brand::get();

        return view('product.index', compact('categories', 'brands', 'product'));
    }
}
