<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\JsonResponse;
use Redirect;
use Response;

class WishlistController extends Controller
{
    public function addToWishlist($id): JsonResponse
    {
        $product = Product::find($id);
        $wishlist = null;

        $data = [];
        $data['id'] = $product->id;
        $data['name'] = $product->title;
        $data['qty'] = 1;
        $data['price'] = $product->price;
        $data['weight'] = 1;
        $data['options']['image'] = $product->image_one;
        $data['options']['product_id'] = $product->id;

        $cartContent = Cart::instance('wishlist')->content();

        $checkItemTrue = $this->checkItemInWishlist($id, $cartContent);

        // если товара нет в wishlist, то добавляет товар
        if (!$checkItemTrue) {
            $wishlist = Cart::instance('wishlist')->add($data);
        } else {
            // удаление из wishlist при повторном нажатии на сердечко
            foreach ($cartContent as $item) {
                if ($item->id == $id) {
                    Cart::instance('wishlist')->remove($item->rowId);
                }
            }
        }

        $countProduct = Cart::instance('wishlist')->count();

        return Response::json([
            'success' => 'added to wishlist',
            'wishlist' => $wishlist,
            'countProduct' => $countProduct,
        ]);
    }

    public function showWishlist()
    {
        $wishlist = Cart::instance('wishlist')->content();
        $categories = Category::whereNull('parent_id')->with('children')->orderBy('name')->get();

        return view('wishlist.index', compact('wishlist', 'categories'));
    }

    public function clearWishlist()
    {
        Cart::instance('wishlist')->destroy();

        $notification = [
            'message' => 'Wishlist удален',
            'alert-type' => 'success'
        ];

        return Redirect::route('home')->with($notification);
    }

    public function removeProduct($id)
    {
        Cart::instance('wishlist')->remove($id);

        $notification = [
            'message' => 'Товар удален из листа желаний',
            'alert-type' => 'success'
        ];

        return Redirect::back()->with($notification);
    }

    public function checkItemInWishlist($id, $cartContent): bool
    {
        $ids = [];
        foreach ($cartContent as $item) {
            $ids[] = $item->id;
        }

        if (in_array($id, $ids)) {
            return true;
        }

        return false;
    }

}
