<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index($id)
    {
        $oneCategory = Category::with('products')->find($id);

        $productsCount = count($oneCategory->products);

        $categories = Category::whereNull('parent_id')->with('children')->orderBy('name')->get();

        $brands = Brand::get();

        return view('category.index', compact('oneCategory', 'categories', 'brands', 'productsCount'));
    }
}
