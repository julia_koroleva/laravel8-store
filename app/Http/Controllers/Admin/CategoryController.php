<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $categories = Category::get();
        $parentCategories = Category::whereNull('parent_id')->get();

        return view('admin.category.index', compact('parentCategories', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:150|unique:categories',
        ]);
        $category = new Category;
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;

        $category->save();

        $notification = [
            'message' => 'Категория успешно добавлена',
            'alert-type' => 'success'
        ];
        return Redirect::back()->with($notification);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param $id
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:150',
        ]);

        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;

        $category->save();

        $notification = [
            'message' => 'Категория успешно обновлена',
            'alert-type' => 'success'
        ];
        return Redirect::to('admin/category')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Category::destroy($id);
        $notification = [
            'message' => 'Категория успешно удалена',
            'alert-type' => 'error'
        ];
        return Redirect::back()->with($notification);

    }
}
