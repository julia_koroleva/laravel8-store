<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view('admin.index');
    }

    public function logout()
    {
        Auth::logout();

        $notification = [
            'message' => 'Вы вышли из аккаунта',
            'alert-type' => 'success'
        ];
        return Redirect::route('home')->with($notification);
    }
}
