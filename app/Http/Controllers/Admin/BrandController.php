<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $brands = Brand::get();

        return view('admin.brand.index', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:2|max:150',
            'logo' => 'required|mimes:jpeg,jpg,png|max:5000'
        ]);
        $brand = new Brand;
        $brand->name = $request->name;

        $image = $request->logo;
        $imageName = date('dmy_H_s_i_').$request->file('logo')->getClientOriginalName();
        $image->move('./images/brands/', $imageName);
        $thumbnail = Image::make('./images/brands/'.$imageName);

        $thumbnail->fit(150, 70);
        $thumbnail->save('./images/brands/thumbnail/'.$imageName);
        $brand->logo = $imageName;

        $brand->save();

        $notification = [
            'message' => 'Бренд успешно добавлен',
            'alert-type' => 'success'
        ];

        return Redirect::back()->with($notification);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:2|max:150',
        ]);

        $brand = Brand::find($request->id);
        $brand->name = $request->name;

        $brand->save();

        $notification = [
            'message' => 'Бренд успешно обновлен',
            'alert-type' => 'success'
        ];

        return Redirect::back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);
        File::delete('./images/brands/thumbnail' .'/'. $brand->logo);
        File::delete('./images/brands' .'/'. $brand->logo);
        $brand->destroy($id);
        $notification = [
            'message' => 'Бренд успешно удален',
            'alert-type' => 'error'
        ];
        return Redirect::back()->with($notification);
    }
}
