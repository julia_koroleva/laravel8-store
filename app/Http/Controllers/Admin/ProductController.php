<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use DB;
use File;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Redirect;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $products = Product::get();

        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories = Category::get();
        $brands = Brand::get();

        return view('admin.product.create', compact('categories', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|min:3|max:150|unique:products',
            'image_one' => 'mimes:jpeg,jpg,png|max:5000'
        ]);

        $product = new Product;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->code = $request->code;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->discount_price = $request->discount_price;

        if ($request->has('image_one')) {
            $image = $request->image_one;
            $imageName = date('dmy_H_s_i_').$request->file('image_one')->getClientOriginalName();
            $image->move('./images/products/', $imageName);
            $thumbnail = Image::make('./images/products/'.$imageName);

            $thumbnail->fit(350, 350);
            $thumbnail->save('./images/products/thumbnail/'.$imageName);
            $product->image_one = $imageName;
        }

        $product->save();

        $notification = [
            'message' => 'Продукт успешно добавлен',
            'alert-type' => 'success'
        ];

        return redirect()->route('product.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $product = Product::find($id);

        return view('admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $product = Product::find($id);

        File::delete('./images/products/thumbnail/'. $product->image_one);
        File::delete('./images/products/'. $product->image_one);
        $product->destroy($id);

        $notification = [
            'message' => 'Продукт успешно удален',
            'alert-type' => 'error'
        ];
        return Redirect::back()->with($notification);

    }
}
