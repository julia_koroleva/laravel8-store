<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Auth;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $categories = Category::whereNull('parent_id')->with('children')->orderBy('name')->get();

        return view('home', compact('categories'));
    }

    /**
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        Auth::logout();
        $notification = [
            'message' => 'Вы вышли из аккаунта',
            'alert-type' => 'success'
        ];
        return Redirect::route('home')->with($notification);
    }
}
