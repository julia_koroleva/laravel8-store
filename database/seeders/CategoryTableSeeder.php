<?php

namespace Database\Seeders;

use App\Models\Category;
use DB;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    static $categories = [
        'Электроника' => [
            'Компьютеры',
            'Планшеты',
            'Комплектующие'
        ],
        'Аудио техника' => [
            'Колонки',
            'Наушники',
            'Плееры'
        ],
        'Видеотехника' => [
            'Телевизоры',
            'ТВ приставки',
            'Игровые приставки'
        ],
        'Телефоны' => [
            'Стационарные телефоны',
            'Кнопочные телефоны',
            'Смартфоны'
        ],
        'Бытовая техника' => [
            'Телевизоры'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::$categories as $category => $subcategories) {
            DB::table('categories')->insert([
                'name' => $category,
                'parent_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            foreach ($subcategories as $subcategory) {
                DB::table('categories')->insert([
                    'name' => $subcategory,
                    'parent_id' => Category::where('name', 'like', $category)->first()->id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }
    }
}
